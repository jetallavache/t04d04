#include <math.h>
#include <stdio.h>

void divmod(int a, int b, int *div, int *mod);
int maxfactor(int a);

int main() {
    int n;
    scanf("%d", &n);
    printf("%d", maxfactor(n));
    return 0;
}

void divmod(int a, int b, int *div, int *mod) {
    if (a < b) {
        *div = 0;
        *mod = a;
        return;
    }
    divmod(a, b + b, div, mod);
    *div += *div;
    if (*mod >= b) {
        *mod -= b;
        ++*div;
    }
}

int maxfactor(int a) {
    if (a < 0) a = a * (-1);

    int i = 2;
    int i2 = 4;
    int max_factor = 0;

    while (i2 <= a) {
        int div;
        int mod;
        divmod(a, i, &div, &mod);
        if (mod == 0) {
            max_factor = i;
            do {
                a = div;
                divmod(a, i, &div, &mod);
            } while (mod == 0);
        }
        i2 += i + i + 1;
        ++i;
        if (i > 3) {
            i2 += i + i + 1;
            ++i;
        }
    }
    if (a > max_factor) max_factor = a;
    return max_factor;
}
