#include <stdio.h>

int _strlen(char *str);
int _strspc(char *str);
int encode(char *str);
int decode(char *str);
 
int main(int argc, char **argv) { 
    
    char str[20];
    fgets(str, 20, stdin);

    //puts(str);
    int simbol = _strlen(str);
    int space = _strspc(str);

    printf(" %d %d\n", simbol, space);
    const char value1 = '0';
    const char value2 = '1';

    if (argv[1][0] == value1) {
        if (space + 1 == simbol)
            encode(str);
        else printf("n/a");
    } 
    if (argv[1][0] == value2) {
        if (space * 2 + 2 == simbol)
            decode(str);
        else printf("n/a");
    } 

    printf("\n___________\n");
 
    for (int i=0; i < argc; i++)
        puts(argv[i]);
}

int _strlen(char *str) {
    int n;
    for (n = 0; *str == 32; str++)
        n++;

    return n;
}

int _strspc(char *str) {
    int n;
    for (n = 0; *str != '\0'; str++)
        n++;
    
    return n;    
}

int encode(char *str) {
    for (int j = 0; j < 20; j++)
      if (str[j] != 32 && str[j] !=10 && str[j] != '\0')
            printf("%hhX ", str[j]);
    return 0;
}

int decode(char *str) {
    for (int j = 0; j < 20; j++)
      if (str[j] != 32 && str[j] !=10 && str[j] != '\0')
            printf("%c ", str[j]);
    return 0;
}
